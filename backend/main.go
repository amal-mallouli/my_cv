package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID          int    `json:"id"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Offre struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	Likes     int       `json:"likes"`
	CreatedAt time.Time `json:"createdat"`
	CandidatID  int       `json:"candidatid"`
}

type Cv struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	Likes     int       `json:"likes"`
	CandidatID  int       `json:"candidatid"`
}

type Like struct {
	ID        int `json:"id"`
	OffreID   int `json:"offreid"`
	CvID      int `json:"cvid"`
	CandidatID  int `json:"candidatid"`
}

var db *sql.DB
var err error

func main() {
	db, err = sql.Open("mysql", "root:xpermedia@tcp(127.0.0.1:3306)/my_cv")

	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	fmt.Println("Successfully connected to MySQL database")

	router := mux.NewRouter()

	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})

	router.HandleFunc("/signup", signUp).Methods("POST")
	router.HandleFunc("/login", login).Methods("POST")
	router.HandleFunc("/profiles", getProfiles).Methods("GET", "OPTIONS")
	router.HandleFunc("/profiles/{id}", getProfile).Methods("GET", "OPTIONS")
	router.HandleFunc("/profiles/{id}", updateProfile).Methods("PUT")
	router.HandleFunc("/profiles/{id}", deleteProfile).Methods("DELETE")
	router.HandleFunc("/offres", getOffres).Methods("GET", "OPTIONS")
	router.HandleFunc("/offres/{id}", getOffre).Methods("GET", "OPTIONS")
	router.HandleFunc("/offres", createOffre).Methods("POST")
	router.HandleFunc("/offres/{id}", updateOffre).Methods("PUT")
	router.HandleFunc("/offres/{id}", deleteOffre).Methods("DELETE")
	router.HandleFunc("/cvs", getCvs).Methods("GET", "OPTIONS")
	router.HandleFunc("/cvs/{id}", getCv).Methods("GET", "OPTIONS")
	router.HandleFunc("/cvs", createCv).Methods("POST")
	router.HandleFunc("/cvs/{id}", updateCv).Methods("PUT")
	router.HandleFunc("/cvs/{id}", deleteCv).Methods("DELETE")
	router.HandleFunc("/likes", getLikes).Methods("GET")
	router.HandleFunc("/likes", createLike).Methods("POST")
	router.HandleFunc("/likes/{id}", getLike).Methods("GET")
	router.HandleFunc("/likes/{id}", deleteLike).Methods("DELETE")

	fmt.Println("Server started on port 9000")
	log.Fatal(http.ListenAndServe(":9000", handlers.CORS(headers, methods, origins)(router)))
}

func signUp(w http.ResponseWriter, r *http.Request) {
	var user User

	json.NewDecoder(r.Body).Decode(&user)

	if user.Email == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Email is missing.")
		return
	}

	if user.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Password is missing")
		return
	} else if len(user.Password) < 6 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Password is too short. Minimum is 6.")
		return
	}

	if user.Name == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Name is blank.")
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)

	if err != nil {
		log.Fatal(err)
	}

	user.Password = string(hash)

	statement, err := db.Prepare("INSERT INTO users (email, password, name, description) VALUES(?, ?, ?, ?)")

	_, err = statement.Exec(user.Email, user.Password, user.Name, user.Description)

	if err != nil {
		log.Fatal(err)
	}

	row := db.QueryRow("SELECT * FROM users ORDER BY id DESC LIMIT 1;")
	err = row.Scan(&user.ID, &user.Email, &user.Password, &user.Name, &user.Description)

	user.Password = ""

	json.NewEncoder(w).Encode(user)
}

func login(w http.ResponseWriter, r *http.Request) {
	var user User

	json.NewDecoder(r.Body).Decode(&user)

	if user.Email == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Email is missing.")
		return
	}

	if user.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Password is missing.")
		return
	} else if len(user.Password) < 6 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Password is too short. Minimum is 6.")
		return
	}

	password := user.Password

	row := db.QueryRow("SELECT * FROM users WHERE email = ?", user.Email)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.Name, &user.Description)

	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("User with this email does not exist.")
			return
		} else {
			log.Fatal(err)
		}
	}

	hashedPassword := user.Password

	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode("Invalid password.")
		return
	}

	user.Password = ""

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
}

func getProfiles(w http.ResponseWriter, r *http.Request) {
	var users []User

	result, err := db.Query("SELECT * FROM users")

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	for result.Next() {
		var user User

		err := result.Scan(&user.ID, &user.Email, &user.Password, &user.Name, &user.Description)

		if err != nil {
			panic(err.Error())
		}

		users = append(users, user)
	}

	json.NewEncoder(w).Encode(users)
}

func getProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	result, err := db.Query("SELECT * FROM users WHERE id = ?", params["id"])

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	var user User

	for result.Next() {
		err := result.Scan(&user.ID, &user.Email, &user.Password, &user.Name, &user.Description)

		if err != nil {
			panic(err.Error())
		}
	}

	if user.ID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Profile not found")
		return
	}

	user.Password = ""

	json.NewEncoder(w).Encode(user)
}

func updateProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("UPDATE users SET name = ?, description = ? WHERE id = ?")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	newName := keyVal["name"]
	newDescription := keyVal["description"]

	if newName == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Name cannot be blank")
		return
	}

	_, err = statement.Exec(newName, newDescription, params["id"])

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "User with ID = %s was updated", params["id"])
}

func deleteProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("DELETE FROM users WHERE id = ?")

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = statement.Exec(params["id"])

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "User with ID = %s was deleted", params["id"])
}

func getOffres(w http.ResponseWriter, r *http.Request) {
	var offres []Offre

	result, err := db.Query("SELECT * FROM offres")

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	for result.Next() {
		var offre Offre

		err := result.Scan(&offre.ID, &offre.CandidatID, &offre.Title, &offre.Content, &offre.Likes, &offre.CreatedAt)

		if err != nil {
			panic(err.Error())
		}

		offres = append(offres, offre)
	}

	json.NewEncoder(w).Encode(offres)
}

func getOffre(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	result, err := db.Query("SELECT * FROM offres WHERE id = ?", params["id"])

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	var offre Offre

	for result.Next() {
		err := result.Scan(&offre.ID, &offre.CandidatID, &offre.Title, &offre.Content, &offre.Likes, &offre.CreatedAt)

		if err != nil {
			panic(err.Error())
		}
	}

	if offre.ID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Offre not found")
		return
	}
	json.NewEncoder(w).Encode(offre)
}

func createOffre(w http.ResponseWriter, r *http.Request) {
	statement, err := db.Prepare("INSERT INTO offres (candidat_id, title, content) VALUES(?, ?, ?)")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	candidatID, err := strconv.Atoi(keyVal["candidat_id"])
	var title string = keyVal["title"]
	var content string = keyVal["content"]

	if title == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Title is blank")
		return
	}

	if content == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Content is blank")
		return
	}

	if candidatID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No candidat")
		return
	}

	_, err = statement.Exec(candidatID, title, content)

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "New offre created.")
}

func updateOffre(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("UPDATE offres SET title = ?, content = ? WHERE id = ?")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	newTitle := keyVal["title"]
	newContent := keyVal["content"]

	if newTitle == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Title cannot be blank")
		return
	}

	if newContent == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Content cannot be blank")
		return
	}

	_, err = statement.Exec(newTitle, newContent, params["id"])

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Offre with ID = %s was updated", params["id"])
}

func deleteOffre(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("DELETE FROM offres WHERE id = ?")

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = statement.Exec(params["id"])

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "Offre with ID = %s was deleted", params["id"])
}
////////////////////

func getCvs(w http.ResponseWriter, r *http.Request) {
	var cvs []Cv

	result, err := db.Query("SELECT * FROM cvs")

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	for result.Next() {
		var cv Cv

		err := result.Scan(&cv.ID, &cv.CandidatID, &cv.Title, &cv.Content, &cv.Likes)

		if err != nil {
			panic(err.Error())
		}

		cvs = append(cvs, cv)
	}

	json.NewEncoder(w).Encode(cvs)
}

func getCv(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	result, err := db.Query("SELECT * FROM cvs WHERE id = ?", params["id"])

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	var cv Cv

	for result.Next() {
		err := result.Scan(&cv.ID, &cv.CandidatID, &cv.Title, &cv.Content, &cv.Likes)

		if err != nil {
			panic(err.Error())
		}
	}

	if cv.ID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Cv not found")
		return
	}
	json.NewEncoder(w).Encode(cv)
}

func createCv(w http.ResponseWriter, r *http.Request) {
	statement, err := db.Prepare("INSERT INTO cvs (candidat_id, title, content) VALUES(?, ?, ?)")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	candidatID, err := strconv.Atoi(keyVal["candidat_id"])
	var title string = keyVal["title"]
	var content string = keyVal["content"]

	if title == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Title is blank")
		return
	}

	if content == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Content is blank")
		return
	}

	if candidatID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No candidat")
		return
	}

	_, err = statement.Exec(candidatID, title, content)

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "New cv created.")
}

func updateCv(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("UPDATE cvs SET title = ?, content = ? WHERE id = ?")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	newTitle := keyVal["title"]
	newContent := keyVal["content"]

	if newTitle == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Title cannot be blank")
		return
	}

	if newContent == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Content cannot be blank")
		return
	}

	_, err = statement.Exec(newTitle, newContent, params["id"])

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Cv with ID = %s was updated", params["id"])
}

func deleteCv(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("DELETE FROM cvs WHERE id = ?")

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = statement.Exec(params["id"])

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "Cv with ID = %s was deleted", params["id"])
}

////////////////////
func getLikes(w http.ResponseWriter, r *http.Request) {
	var likes []Like

	result, err := db.Query("SELECT * FROM likes")

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	for result.Next() {
		var like Like

		err := result.Scan(&like.ID, &like.OffreID, &like.CvID, &like.CandidatID)

		if err != nil {
			panic(err.Error())
		}

		likes = append(likes, like)
	}

	json.NewEncoder(w).Encode(likes)
}

func createLike(w http.ResponseWriter, r *http.Request) {
	statement, err := db.Prepare("INSERT INTO likes (offreid, cvid, candidatid) VALUES(?, ?, ?)")

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	candidatID, err := strconv.Atoi(keyVal["candidatid"])
	offreID, err := strconv.Atoi(keyVal["offreid"])
	cvID, err := strconv.Atoi(keyVal["offreid"])

	if candidatID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No candidat")
		return
	}

	if offreID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No offre")
		return
	}

	_, err = statement.Exec(offreID, candidatID)

	if err != nil {
		log.Fatal(err)
		return
	}
	if cvID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No cv")
		return
	}

	_, err = statement.Exec(cvID, candidatID)

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "New like created.")
}

func getLike(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	result, err := db.Query("SELECT * FROM likes WHERE id = ?", params["id"])

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	var like Like

	for result.Next() {
		err := result.Scan(&like.ID, &like.OffreID, &like.CvID, &like.CandidatID)

		if err != nil {
			panic(err.Error())
		}
	}

	if like.ID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Like not found")
		return
	}

	json.NewEncoder(w).Encode(like)
}

func deleteLike(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	statement, err := db.Prepare("DELETE FROM likes WHERE id = ?")

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = statement.Exec(params["id"])

	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, "Offre with ID = %s was deleted", params["id"])
}
