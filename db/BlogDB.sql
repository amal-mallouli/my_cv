-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


 
 


--------------------------------------------

--
-- Table structure for table `cvs`
--
DROP TABLE IF EXISTS `cvs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cvs` (
  `cvid` int NOT NULL AUTO_INCREMENT,
  `candidat_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  `likes` int DEFAULT '0',
  `experienceid` int NOT NULL,
  `competenceid` int NOT NULL,
  `formationid` int NOT NULL,
  `langueid` int NOT NULL,
  'candidatureid' int NOT NULL,
  PRIMARY KEY (`cvid`),
  KEY `fk_user_1` (`candidat_id`),
  CONSTRAINT `fk_user_1` FOREIGN KEY (`candidat_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE


) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cvs`
--

LOCK TABLES `cvs` WRITE;
/*!40000 ALTER TABLE `cvs` DISABLE KEYS */;
INSERT INTO `cvs` VALUES (1,1,'Ingenieur DevOps','layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)',0,'2021-02-05 18:57:22');
/*!40000 ALTER TABLE `cvs` ENABLE KEYS */;
UNLOCK TABLES;

--------------------------------------------


-- Table structure for table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experiences` (
  `experienceid` int NOT NULL AUTO_INCREMENT,
  `cv_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  `date_debut` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_fin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`experienceid`),
  KEY `fk1_cv` (`cv_id`),
  CONSTRAINT `fk1_cv` FOREIGN KEY (`cv_id`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE

) ;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `experiences`
--

LOCK TABLES `experiences` WRITE;
/*!40000 ALTER TABLE `experiences` DISABLE KEYS */;
INSERT INTO `experiences` VALUES (1,1,'Stage PFE chez SASTEC','j ai participé a coder une Application web dudié pour les restaurant','2020-02-05 18:57:22','2021-02-05 18:57:22');
/*!40000 ALTER TABLE `experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
--
---------------------------------------
-- Table structure for table `competences`
--

DROP TABLE IF EXISTS `competences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `competences` (
  `competenceid` int NOT NULL AUTO_INCREMENT,
  `cv_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,


  PRIMARY KEY (`competenceid`),
  KEY `fk2_cv` (`cv_id`),
  CONSTRAINT `fk2_cv` FOREIGN KEY (`cv_id`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE

) ;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `competences`
--

LOCK TABLES `competences` WRITE;
/*!40000 ALTER TABLE `competences` DISABLE KEYS */;
INSERT INTO `competences` VALUES (1,1,'JaVa',' Application web smart Menu codé avec JAVA ');
/*!40000 ALTER TABLE `competences` ENABLE KEYS */;
UNLOCK TABLES;

--
--
-------------------------------
-- Table structure for table `formations`
--

DROP TABLE IF EXISTS `formations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `formations` (
  `formationid` int NOT NULL AUTO_INCREMENT,
  `cv_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `ecole` mediumtext NOT NULL,
  `diplome` mediumtext NOT NULL,


  PRIMARY KEY (`formationid`),
  KEY `fk3_cv` (`cv_id`),
  CONSTRAINT `fk3_cv` FOREIGN KEY (`cv_id`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE

) ;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `formations`
--

LOCK TABLES `formations` WRITE;
/*!40000 ALTER TABLE `formations` DISABLE KEYS */;
INSERT INTO `formations` VALUES (1,1,'Ingenieur en télécommunications',' Ecole nationale d electroniques et telecommunication de sfax ',' Diplome en ingenierie reseaux et telecommunication ');
/*!40000 ALTER TABLE `formations` ENABLE KEYS */;
UNLOCK TABLES;

--
--
-------------------------------
-- Table structure for table `langues`
--

DROP TABLE IF EXISTS `langues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `langues` (
  `langueid` int NOT NULL AUTO_INCREMENT,
  `cv_id` int NOT NULL,
  `langue` varchar(200) NOT NULL,
  `niveau` varchar(200) NOT NULL,
  `certif` varchar(200) NOT NULL,
  PRIMARY KEY (`langueid`),
  KEY `fk4_cv` (`cv_id`),
  CONSTRAINT `fk4_cv` FOREIGN KEY (`cv_id`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE

) ;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `langues`
--

LOCK TABLES `langues` WRITE;
/*!40000 ALTER TABLE `langues` DISABLE KEYS */;
INSERT INTO `langues` VALUES (1,1,'Français',' Niveau 2 ',' Diplome DELF Niveau 2');
/*!40000 ALTER TABLE `langues` ENABLE KEYS */;
UNLOCK TABLES;

--
--
-------------------------------
-- Table structure for table `candidatures`
--

DROP TABLE IF EXISTS `candidatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `candidatures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `candidat_id` int NOT NULL,
  `cv_id` int NOT NULL,
  `rh_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `likes` int DEFAULT '0',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_user` (`candidat_id`),
  KEY `fk_rh1` (`rh_id`),
  KEY `fk_cv2` (`cv_id`),
  CONSTRAINT `fk_cv2` FOREIGN KEY (`cv_id`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_candidat1` FOREIGN KEY (`candidat_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rh1` FOREIGN KEY (`rh_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidatures`
--

LOCK TABLES `candidatures` WRITE;
/*!40000 ALTER TABLE `candidatures` DISABLE KEYS */;
INSERT INTO `candidatures` VALUES (1,1,1,1,'Ingenieur DevOps',0,'2021-02-05 18:57:22');
/*!40000 ALTER TABLE `candidatures` ENABLE KEYS */;
UNLOCK TABLES;

--
--
------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `offreid` int NOT NULL,
  `cvid` int NOT NULL,
  `candidatid` int NOT NULL,
    `candidatureid` int NOT NULL,
  `rhid` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `offreid` (`offreid`),
  KEY `candidatid` (`candidatid`),
  KEY `rhid` (`rhid`),
  KEY `cvid` (`cvid`),
  KEY `candidatureid` (`candidatureid`),
    CONSTRAINT `likes_ibfk_5` FOREIGN KEY (`candidatureid`) REFERENCES `candidatures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likes_ibfk_3` FOREIGN KEY (`cvid`) REFERENCES `cvs` (`cvid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`offreid`) REFERENCES `offres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`candidatid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `likes_ibfk_4` FOREIGN KEY (`rhid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (2,11,5,2,1,3);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offres`
--

DROP TABLE IF EXISTS `offres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `offres` (
  `id` int NOT NULL AUTO_INCREMENT,
  `candidat_id` int NOT NULL,
  `rh_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  `likes` int DEFAULT '0',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_user` (`candidat_id`),
  KEY `fk_rh` (`rh_id`),
  CONSTRAINT `fk_user` FOREIGN KEY (`candidat_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rh` FOREIGN KEY (`rh_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offres`
--

LOCK TABLES `offres` WRITE;
/*!40000 ALTER TABLE `offres` DISABLE KEYS */;
INSERT INTO `offres` VALUES (1,1,1,'Ingenieur DevOps','Sastec offre une opportinité ingenieur devops ..',0,'2021-02-05 18:57:22');
/*!40000 ALTER TABLE `offres` ENABLE KEYS */;
UNLOCK TABLES;

--
--


-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'amalmallouli@example.com','password','Amal Mallouli','Etudiante en master'),(2,'mariemmallouli@example.com','123456','Mariem Mallouli','RH'));
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-06  9:51:58


