import React, {useState, useEffect,} from 'react'
import axios from 'axios'
import {Button, Form} from 'react-bootstrap'
import { formatDateFull } from "../utils";
import { useHistory } from "react-router-dom";

export default function CvDetailComponent(props) {
    const [cv, setCv] = useState({})
    const [name, setName] = useState('')
    const [likes, setLikes] = useState([])
    const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")))
    const [likeId, setLikeId] = useState(-1)

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const [editMode, setEditMode] = useState(false)

    let history = useHistory()

    useEffect(() => {
        getCv()
    }, [])

    console.log(likeId);

    const deleteCv = () => {
        if (window.confirm("Delete this cv?")) {
            axios.delete(axios.defaults.baseURL + `cvs/${props.id}`).then(res => {
                history.push(`/profiles/${user.id}`)
            }).catch(err => console.log(err))
        }
    }

    const getCv = () => {
        axios.get(axios.defaults.baseURL + `cvs/${props.id}`).then(res => {
            setCv(res.data);
            setTitle(res.data.title)
            setContent(res.data.content)

            axios.get(axios.defaults.baseURL + `profiles/${res.data.candidatid}`).then(r => {
                setName(r.data.name)
            })

            axios.get(axios.defaults.baseURL + "likes").then(r => {
                setLikes(r.data.filter(like => like.cvid == res.data.id))

                if (r.data.find(like => like.cvid == res.data.id && like.candidatid == user.id))
                    setLikeId(r.data.find(like => like.cvid == res.data.id && like.candidatid == user.id).id)
                else
                    setLikeId(-1)
            }).catch(err => console.log(err))
        }).catch(err => {
            history.push("/")
        })
    }

    const like = () => {
        axios.post(axios.defaults.baseURL + "likes", {
            cvid: cv.id.toString(),
            candidatid: user.id.toString()
        }).then(res => {
            getCv()
        }).catch(err => console.log(err.response.data))
    }

    const renderCv = () => {
        if (editMode) {
            return null
        } else {
            return (
                <div style={{width: "70%", margin: "auto", marginTop: 64}}>
                    <h1>{cv.title}</h1>
                    <p><a href={`/profiles/${cv.candidatid}`}>{name}</a></p>
                    <small>{formatDateFull(cv.createdat)}</small>
                    <hr />
                    <div style={{display: "flex"}}>
                        <div style={{flex: 1, textAlign: "center"}}>
                            <h6>{likes.length} likes</h6>
                            {renderCvButtons()}
                        </div>
                        <div style={{flex: 8}}>
                        {cv.content}
                        </div>
                    </div>
                </div>
            )
        }
    }

    const renderEditCvForm = () => {
        if (editMode) {
            return (
                <Form style={{width: "70%", margin: "64px auto"}} onSubmit={updateCv}>
                    <Form.Group>
                        <Form.Control 
                            onChange={e => setTitle(e.target.value)}
                            value={title}
                            required={true}
                            style={{
                                fontSize: 36,
                                color: "black",
                                fontWeight: 600
                            }}
                        />
                    </Form.Group>
                    <p><a href={`/profiles/${cv.candidatid}`}>{name}</a></p>
                    <small>{formatDateFull(cv.createdat)}</small>
                    <hr />
                    <div style={{display: "flex"}}>
                        <div style={{flex: 1, textAlign: "center"}}>
                            <h6>133 Likes</h6>
                            <Button variant="success" style={{margin: "8px 0"}} type="submit">Confirm</Button>
                            <Button variant="dark" style={{margin: "8px 0"}} onClick={() => setEditMode(false)}>Cancel</Button>
                        </div>
                        <Form.Group style={{flex: 8}}>
                            <Form.Control 
                                onChange={e => setContent(e.target.value)}
                                value={content}
                                required={true}
                                as="textarea"
                                style={{
                                    color: "black",
                                    height: "150%"
                                }}
                            />
                        </Form.Group>
                    </div>
                </Form>
            )
        } else {
            return null
        }
    }

    const renderCvButtons = () => {
        if (user == null)
            return null
        else if (cv.candidatid == user.id) {
            return (
                <div>
                    <Button variant="primary" style={{margin: "8px 0"}} onClick={() => setEditMode(true)}>Edit</Button>
                    <Button variant="danger" style={{margin: "8px 0"}} onClick={deleteCv}>Delete</Button>
                </div>
            )
        } else {
            if (likeId != -1) {
                return (
                    <Button variant="light" style={{margin: "8px 0"}} onClick={unlike}>Liked</Button>
                )
            } else {
                return (
                    <Button variant="success" style={{margin: "8px 0"}} onClick={like}>Like</Button>
                )
            }
        }
    }

    const updateCv = (e) => {
        e.preventDefault()

        axios.put(axios.defaults.baseURL + `cvs/${props.id}`, {
            title: title,
            content: content
        }).then(res => {
            setEditMode(false)
            getCv()
        }).catch(err => {
            console.log(err.response.data);
        })
    }

    const unlike = () => {
        axios.delete(axios.defaults.baseURL + `likes/${likeId}`).then(res => {
            getCv()
        }).catch(err => console.log(err))
    }

    return (
        <div>
            {renderCv()}
            {renderEditCvForm()}
        </div>
    )
}
