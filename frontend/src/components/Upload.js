import axios from 'axios';
import React,{Component} from 'react'; 
class Upload extends Component { 

    state = { 
  
      // Initially, no file is selected 
      selectedFile: null
    }; 
     
    // On file select (from the pop up) 
    onFileChange = event => { 
      // Update the state 
      this.setState({ selectedFile: event.target.files[0] }); 
    }; 
     
    // On file upload (click the upload button) 
    onFileUpload = () => { 
      // Create an object of formData 
      const formData = new FormData(); 
     
      // Update the formData object 
      formData.append( 
        "cv", 
        this.state.selectedFile, 
        this.state.selectedFile.name 
      ); 
     
      // Details of the uploaded file 
      console.log(this.state.selectedFile); 
     
      // Request made to the backend api 
      // Send formData object 
      axios.post("api/uploadfile", formData); 
    }; 
     
    // File content to be displayed after 
    // file upload is complete 
    fileData = () => { 
      if (this.state.selectedFile) { 
          
        return ( 
          <div> 
              
            <h2>File Details:</h2> 
            <p>File Name: {this.state.selectedFile.name}</p> 
            <p>File Type: {this.state.selectedFile.type}</p> 
            <p> 
              Last Modified:{" "} 
              {this.state.selectedFile.lastModifiedDate.toDateString()} 
            </p> 
          </div> 
        ); 
      } else { 
        return ( 
          <div> 
              <div style={{display: "flex"}}>
                        <div style={{flex: 1, textAlign: "center"}}>
                            <div></div>
            <br /> 
            <h4>Choisissez avant d'appuyer sur le bouton Télécharger</h4> 
            </div>
                </div>
          </div> 
        ); 
      } 
    }; 
     
    render() { 
      return ( 
        <div> 
            <div style={{display: "flex"}}>
                        <div style={{flex: 1, textAlign: "center"}}>
                            <div>
            <h1> 
              Ajouter votre CV
            </h1> 

            </div>
            
            <h3> 
              Télechargez votre cv
            </h3> 
            <div> 
            
                <input type="file" onChange={this.onFileChange} /> 
                <button onClick={this.onFileUpload}> 
                  Upload! 
                </button> 
                
                            
                </div>
                </div>
                        
                        
            </div> 
          {this.fileData()} 
        </div> 
      ); 
    } 
  } 
  
  export default Upload; 