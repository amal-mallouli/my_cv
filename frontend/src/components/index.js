import RegisterComponent from "./RegisterComponent"
import LoginComponent from "./LoginComponent"
import HomePage from "./HomePage"
import OffreDetailComponent from "./OffreDetailComponent"
import CvDetailComponent from "./CvDetailComponent"
import UserDetailComponent from "./UserDetailComponent"
import NavbarComponent from "./NavbarComponent"
import CreateOffreComponent from "./CreateOffreComponent"
import CreateCvComponent from "./CreateCvComponent"
import Upload from "./Upload"

export {RegisterComponent, LoginComponent, HomePage, OffreDetailComponent, CvDetailComponent, UserDetailComponent, NavbarComponent, CreateOffreComponent, CreateCvComponent , Upload}